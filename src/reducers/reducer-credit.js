export default function() {
  return [
    { name: "Total Accounts: 6", info: "2 accounts are high interest", warn: "warn" },
    { name: "Length of Credit", info: "6 years", warn: "" },
    { name: "Inquiries", info: "None", warn: "" },
    { name: "Revolving Utilization", info: "15%", warn: "" },
    { name: "Missed Payments", info: "None", warn: "" }
  ];
}
