import { combineReducers } from 'redux';
import CreditReducer from './reducer-credit';
import ScoreReducer from './reducer-score';

const rootReducer = combineReducers({
  credit: CreditReducer,
  score: ScoreReducer
});

export default rootReducer;
