import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Credit from './components/credit';
import Consent from './components/consent';

const Routes = () => (
    <Switch>
        <Route exact path='/' component={Consent} />
        <Route path='/consent' component={Consent} />
        <Route path='/credit' component={Credit} />
        <Route path='*' component={Consent} />
    </Switch>
);

export default Routes;
