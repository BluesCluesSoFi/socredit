import React, { Component } from 'react';
import { connect } from 'react-redux';
import CreditDetail from '../components/credit-detail';

class CreditDetailList extends Component {
  // render a single credit detail item
  renderCreditDetail(detail) {
    const name = detail.name;
    const info = detail.info;
    const warn = detail.warn;
    // longer form description in future?

    return (
      <CreditDetail key={name} name={name} info={info} warn={warn} />
    );
  }

  render() {
    return (
      <div id="credit-detail-list" className="credit">
        <h5>Details</h5>
        {this.props.credit.map(this.renderCreditDetail)}
      </div>
    );
  }
}

function mapStateToProps({ credit }) {
  return { credit };
}

export default connect(mapStateToProps)(CreditDetailList);
