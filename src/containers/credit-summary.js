import React, { Component } from 'react';
import { connect } from 'react-redux';

import CreditScore from '../components/credit-score';
import CreditBar from '../components/credit-bar';
import CreditXSell from '../components/credit-cross-sell';

class CreditSummary extends Component {
  render() {
    return (
      <div id="credit-summary" className="credit">
        <CreditScore score={this.props.score.score} strength={this.props.score.strength} />
        <CreditBar score={this.props.score.score} strength={this.props.score.strength} />
        <CreditXSell number={this.props.score.number} />
      </div>
    );
  }
}

function mapStateToProps({ score }) {
  return { score };
}

export default connect(mapStateToProps)(CreditSummary);
