import React, { Component } from 'react';

export default (props) => {
  return (
    <div id="credit-score">
      <h5>Your Credit Score</h5>
      <h1 id="score">{props.score}</h1>
      <p>{props.strength}</p>
    </div>
  );
}
