import React, { Component } from 'react';

export default (props) => {
  return (
    <div className="credit-cross-sell">
      <div>
        <p id="bulb" className="decorated">{props.number} of your credit accounts have high balances</p>
      </div>
      <p className="small">You could pay off your high-interest credit card debt with a personal loan at a lower rate.</p>
      <a href="https://kraken-dev-99.sofitest.com/personal-loan-service/s/app/version/route/2">
        <button className="btn">Learn more</button>
      </a>
    </div>
  );
}
