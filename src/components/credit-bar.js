import React, { Component } from 'react';
import { Line } from 'rc-progress';

function color(strength) {
  switch (strength) {
    case 'Very Good':
      return '#7ED321';
    case 'OK':
      return '#F5A623';
    case 'Needs Improvement':
      return '#D0021B';
    default:
      return '#000000';
  }
}

function percent(score) {
  return (score - 300)/550 * 100;
}

export default (props) => {
  return (
    <div id="credit-bar">
      <Line percent={percent(props.score)} strokeWidth="4" strokeColor={color(props.strength)} />
      <span>300</span>
      <span>850</span>
    </div>
  );
}
