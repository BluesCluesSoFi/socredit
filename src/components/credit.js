import React, { Component } from 'react';
import CreditDetailList from "../containers/credit-detail-list";
import CreditSummary from "../containers/credit-summary";
import {Provider} from "react-redux";

import reducers from '../reducers';

export default class Credit extends Component {
    render() {
        return (
            <div>
                <img id="navbar" src="../../assets/navbar.png" />
                <CreditSummary />
                <CreditDetailList />
            </div>
        );
    }
}
