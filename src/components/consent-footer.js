import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class ConsentFooter extends Component {
    render() {
        return (
            <div className="consent-footer">
                <div className="line"/>
                <Link to='/credit'>
                    <button className="consent-btn">Let's do this</button>
                </Link>
            </div>
        );
    }
}

