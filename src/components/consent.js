import React, { Component } from 'react';
import ConsentBody from './consent-body';
import ConsentFooter from './consent-footer';

export default class Consent extends Component {
    render() {
        return (
            <div>
                <img id="navbar" src="../../assets/navbar.png" />
                <ConsentBody />
                <ConsentFooter />
            </div>
        );
    }
}
