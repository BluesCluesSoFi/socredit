import React, { Component } from 'react';

export default (props) => {
  return (
    <div className="credit-detail">
      <div className="decorated">
        <p>{props.name}<br/>
        <span className={props.warn}>{props.info}</span></p>
      </div>
    </div>
  );
}
