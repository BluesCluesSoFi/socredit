import React, { Component } from 'react';

export default class ConsentBody extends Component {
    render() {
        return (
            <div className="consent-body">
                <p className="great-credit-score">
                    A great credit score can make a big difference in money when it comes to…
                </p>
                <table className="products">
                    <tbody>
                        <tr>
                            <td className="product-img">
                                <img className="large" src="/assets/education.svg"/>
                            </td>
                            <td className="product-img img-center">
                                <img className="large" src="/assets/home.svg"/>
                            </td>
                            <td className="product-img">
                                <img className="large" src="/assets/auto.svg"/>
                            </td>
                        </tr>
                        <tr>
                            <td className="product-names">
                                Student Loans
                            </td>
                            <td className="product-names">
                                Mortgage Rates
                            </td>
                            <td className="product-names">
                                Car Loans
                            </td>
                        </tr>
                    </tbody>
                </table>
                <a className="learn-more">
                    Learn More
                </a>
                <div className="line" />
                <p className="get-your-credit-score">
                    Get your credit score now as a free benefit for being a SoFi Member.
                </p>
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <img className="small" src="/assets/life-insurance.svg"/>
                            </td>
                            <td>
                                <p className="benefit-items">
                                    This is not a hard pull on your credit and won’t affect your credit score.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img className="small" src="/assets/lock.svg"/>
                            </td>
                            <td>
                                <p className="benefit-items">
                                    You can get your credit score without lifting a credit freeze.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img className="small" src="/assets/green-card.svg"/>
                            </td>
                            <td>
                                <p className="benefit-items">
                                    You consent to using your name, date of birth, and your address to get your credit score.
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

